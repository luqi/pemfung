> data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr
> 	| V String | Let String Expr Expr
> 	deriving Show

% 1. 
% Fungsi map

> mapExpr f (C c) = C (f c)
> mapExpr f (e1 :+ e2) = mapExpr f e1 :+ mapExpr f e2
> mapExpr f (e1 :- e2) = mapExpr f e1 :- mapExpr f e2
> mapExpr f (e1 :* e2) = mapExpr f e1 :* mapExpr f e2
> mapExpr f (e1 :/ e2) = mapExpr f e1 :/ mapExpr f e2
> mapExpr f (V _) = C (f 0.0)

% Fungsi fold

> foldExpr (C x) = x
> foldExpr ((C x) :- (C y)) = foldl (-) 0 [x, y]
> foldExpr ((C x) :+ (C y)) = foldl (+) 0 [x, y]
> foldExpr ((C x) :* (C y)) = foldl (*) 0 [x, y]
> foldExpr ((C x) :/ (C y)) = foldl (/) 0 [x, y]



% 2. Fungsi evaluate

> evaluate :: Expr -> Float
> evaluate (C x) = x
> evaluate (e1 :+ e2) = evaluate e1 + evaluate e2
> evaluate (e1 :- e2) = evaluate e1 - evaluate e2
> evaluate (e1 :* e2) = evaluate e1 * evaluate e2
> evaluate (e1 :/ e2) = evaluate e1 / evaluate e2
> evaluate (Let v e0 e1) = evaluate (subst v e0 e1)
> evaluate (V _) = 0.0


> subst :: String -> Expr -> Expr -> Expr
> subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
> subst _ _ (C c) = (C c)
> subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
> subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
> subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
> subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
> subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

% 5. Fungsi evaluate menggunakan fold



% 10. Apakah anda sempat mempelajari teknik De Bruijn Index yang efisien
% Tidak sempat