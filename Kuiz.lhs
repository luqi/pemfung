% 1.

> maxTiga :: Int -> Int -> Int -> Int
> maxTiga a b c = max (max a b) c

% code diatas menghasilkan hasil yang benar sesuai spesifikasi soal


% 2.

> a = [(x,y) | x <- [1..4], y <- [2..6], x*2 == y]

% a = [(1,2), (2,4), (3,6)]


% 3.

> quicksort [] = []
> quicksort (x:xs) = (quicksort [y | y <- xs, y <= x]) ++ [x] ++ (quicksort [y | y <- xs, y > x])


% 4.

> jumlahList (x:xs) = foldl (+) 0


% 6.

> primes = sieve [2..]
> 	where sieve (x:xs) = x : (sieve [y | y <- xs, y `mod` x /= 0])


% 7.

% flip :: (a -> b -> c) -> b -> a -> c

> flip f x y = f y x


% 8.

% updateState :: (RobotState -> RobotState) -> Robot ()


% 9.

% turnRight = updateState (\s -> s {facing = right (facing s)})


% 10.

% toEnum :: Enum a => Int -> a
% Hal ini terjadi karena toEnum tetaplah fungsi toEnum yang dapat mengembalikan tipe data apapun. Namun karena fungsi right didefinisikan mengembalikan tipe data Direction, maka toEnum di dalam fungsi ini akan menghasilkan tipe data Direction.